class ListsController < ApplicationController
  before_action :authenticate_user!

  def new
    @list = List.new
  end

  def show
    @list = List.find(id)

    unless @list.created_by?(current_user) || @list.public_list?
      redirect_to root_path
    end
  end

  def create
    @list = current_user.lists.new(list_params)

    if @list.save
      redirect_to list_path(@list)
    else
      render :new
    end
  end

  def duplicate_and_make_private
    @list = List.find(id)

    if @list.public_list?
      new_list              = @list.duplicate_and_make_private_to(current_user)
      new_list.save
      redirect_to list_path(new_list)
    else
      redirect_to root_path
    end
  end

  def personal
    @title = 'My task'
    @lists = current_user.lists

    render 'lists/shared_list'
  end

  def public
    @title = 'Public'
    @lists = List.all_public_lists

    render 'lists/shared_list'
  end

  def favorites
    @title = 'Favorite'
    @lists = current_user.favorite_lists.map { |list| Struct.new(:id, :name).new(list.list_id, list.list_name) }

    render 'lists/shared_list'
  end

  private

  def list_params
    params.require(:list).permit(:name, :public_list)
  end

  def id
    params[:id]
  end
end
