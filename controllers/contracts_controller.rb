class Client::ContractsController < ApplicationController
  # -------------------------------------------------------------------------------------

  before_action :authenticate_user!
  authorize_resource :class => "Contract::Contract"

  responders :flash
  respond_to :html

  def info_for_paper_trail
    { ip: request.remote_ip, identifier: 'contract' }
  end

  # -------------------------------------------------------------------------------------

  before_action :set_client, except: [:all]
  before_action :set_contract, only: [:show, :edit, :update, :destroy]

  #-------------------------------------------------------------------------------------

  def index
    redirect_to client_client_path(@client)
  end

  def all
    @contracts = Contract::Contract.includes(:client, :representative, :telemarketing, :client_company).order('signature_date DESC').page params[:page]
  end

  def show
    @logs       = Version.where(identifier: 'contract').where(father_id: params[:id]).includes(:user).order('created_at DESC')
  end

  def new
    @contract = @client.contracts.build
    @item     = @contract.items.build
    @item.item_editions.build
    @item.item_cities.build
    @item.item_segments.build
  end

  def edit
    @banks = Financial::Bank.all
  end

  def create
    clean_price

    @contract = @client.contracts.new(contract_contract_params)
    if @contract.save
      respond_with(@contract, location: client_client_contract_path(@client, @contract))
    else
      respond_with(@contract)
    end
  end

  def update
    clean_price

    @contract = @client.contracts.find(params[:id])
    @contract.update(contract_contract_params)
    respond_with(@contract, location: client_client_contract_path(@client, @contract))
  end

  def destroy
    if @contract.active
      active = false
    else
      active = true
    end

    @contract.update_attribute(:active, active)
    flash[:notice] = "Contrato #{@contract.number} foi #{(active ? "ativado" : "desativo")} com sucesso!"
    respond_with(@contract, location: client_client_path(@client))
  end

  private
    def set_client
      @client = Client::Client.find(params[:client_id])
    end

    def set_contract
      @contract = Contract::Contract.find(params[:id])
    end

    def clean_price
      if params[:contract_contract][:price].include?(",")
        params[:contract_contract][:price] = params[:contract_contract][:price].to_s.gsub(/\./, '').gsub(/,/, '.')
      end

      if params[:contract_contract][:payments_attributes]
        params[:contract_contract][:payments_attributes].each do |chave, valor|
          valor[:price] = valor[:price].to_s.gsub(/\./, '').gsub(/,/, '.') if valor[:price].include?(",")
        end
      end
    end

    def contract_contract_params
      params.require(:contract_contract).permit(
        :client_id, :client_company_id, :representative_id, :telemarketing_id, :status, :number, :free, :price, :parcels_number, :first_parcel_date, :signature_date, :observation,
        :items_attributes => [
          :_destroy, :id, :product_id, :product_specification_id, :descriptive,
          :item_cities_attributes     => [:_destroy, :id, :country_id, :state_id, :city_id, :default],
          :item_editions_attributes   => [:_destroy, :id, :product_edition_id],
          :item_segments_attributes   => [:_destroy, :id, :segment_id, :default]
        ],
        :payments_attributes        => [
          :_destroy, :id, :parcel_number, :price, :payable_type, :parcel_date,
          :check_bank_id, :check_agency, :check_account, :check_number,
          :deposit_bank_id, :deposit_account,
          :creditcard_portions, :creditcard_flag_type,
          :slip_configuration_id
        ]
      )
    end
end
