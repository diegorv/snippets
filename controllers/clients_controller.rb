class Client::ClientsController < ApplicationController
  # -------------------------------------------------------------------------------------

  include BasicSettings

  def info_for_paper_trail
    { ip: request.remote_ip, identifier: 'client' }
  end

  # -------------------------------------------------------------------------------------

  before_action :set_client, only: [:show, :edit, :update, :destroy]

  # -------------------------------------------------------------------------------------

  def index
    @clients = Client::Client.includes(:companies).order('created_at DESC').page params[:page]
  end

  def show
    @logs      = Version.where(identifier: 'client').where(father_id: params[:id]).includes(:user).order('created_at DESC')
  end

  def new
    @client   = Client::Client.new
    @company  = @client.companies.build
  end

  def create
    @client = Client::Client.new(client_client_params)
    @client.save
    respond_with(@client)
  end

  def edit
  end

  def update
    @client.update(client_client_params)
    respond_with(@client)
  end

  def destroy
    unless @client.destroy
      flash[:alert] = "Não é possível deletar um cliente com contratos atrelados"
    end

    respond_with(@client)
  end

  private
    def set_client
      @client = Client::Client.find(params[:id])
    end

    def client_client_params
      params.require(:client_client).permit(
        :active, :client_type, :priority_type, :document_head, :document_optional,
        :companies_attributes => [
          :_destroy, :id, :company_name, :view_name,
          :locations_attributes => [
            :_destroy, :id, :address_type, :country_id, :zipcode, :state_id, :city_id, :street_name, :number, :post_office_box, :complement,
            :neighbordhood, :observation, :visible_internet, :visible_diagramming, :default, :latitude, :longitude
          ],
          :contacts_attributes  => [:_destroy, :id, :contact_type, :value, :visible_internet, :visible_diagramming],
          :employees_attributes => [:_destroy, :id, :employee_type, :name, :birth_date, :masonic, :visible_internet, :visible_diagramming]
        ]
        )
    end

  # -------------------------------------------------------------------------------------
end
