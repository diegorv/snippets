require 'spec_helper'

describe MenuHelper do
  context "Função menu_li" do
    name        = "Administrar usuários"
    id_name     = "menu_administrar_usuarios"
    url_passed  = "url_path"
    icone       = "fontello-icon-trash-5"

    it "apenas com url" do
      html_return = "<li id=\"#{id_name}\"><a href=\"#{url_passed}\"><i class=\"im-arrow-right3\"></i><span class='txt'>#{name}</span></a></li>"

      expect(helper.menu_li(name, url_passed)).to eql(html_return)
    end

    it "apenas com url e icone" do
      html_return = "<li id=\"#{id_name}\"><a href=\"#{url_passed}\"><i class=\"#{icone}\"></i><span class='txt'>#{name}</span></a></li>"

      expect(helper.menu_li(name, url_passed, icone)).to eql(html_return)
    end
  end
end
