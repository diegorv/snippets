require 'spec_helper'

describe ButtonHelper do
  context "Função button_tag" do
    name        = "Adicionar Contrato"
    id_name     = "btn_adicionar_contrato"
    url_passed  = "url_path"

    it "apenas com url" do
      html_return = "<a id=\"#{id_name}\" class=\"btn btn-default btn-sm\" href=\"#{url_passed}\">#{name}</a>"

      expect(helper.button_tag(name, url_passed)).to eql(html_return)
    end

    it "com url e diversas cores definidas" do
      valid_values = [
        [:default    , 'btn-default'],
        [:primary    , 'btn-primary'],
        [:info       , 'btn-info'   ],
        [:success    , 'btn-success'],
        [:warning    , 'btn-warning'],
        [:danger     , 'btn-danger' ],
        [:black      , 'btn-inverse'  ],
        [:back       , 'btn-white']
      ]
      valid_values.each do |name_cor, value|
        html_return = "<a id=\"#{id_name}\" class=\"btn #{value} btn-sm\" href=\"#{url_passed}\">#{name}</a>"
        expect(helper.button_tag(name, url_passed, name_cor)).to eql(html_return)
      end
    end

    it "com url e cor definida" do
      html_return = "<a id=\"#{id_name}\" class=\"btn btn-primary btn-sm\" href=\"#{url_passed}\">#{name}</a>"

      expect(helper.button_tag(name, url_passed, :primary)).to eql(html_return)
    end

    it "com url, cor e ícone definidos" do
      html_return = "<a id=\"btn_adicionar_contrato\" class=\"btn btn-primary btn-sm\" href=\"url_path\"><i class=\"fa-chart-up\"></i> Adicionar Contrato</a>"
      expect(helper.button_tag(name, url_passed, :primary, "fa-chart-up")).to eql(html_return)

      html_return = "<a id=\"btn_adicionar_contrato\" class=\"btn btn-primary btn-sm\" href=\"url_path\"><i class=\"fa-icon-down-thin\"></i> Adicionar Contrato</a>"
      expect(helper.button_tag(name, url_passed, :primary, "fa-icon-down-thin")).to eql(html_return)

      html_return = "<a id=\"btn_adicionar_contrato\" class=\"btn btn-primary btn-sm\" href=\"url_path\"><i class=\"fax-icon-down-thin\"></i> Adicionar Contrato</a>"
      expect(helper.button_tag(name, url_passed, :primary, "fax-icon-down-thin")).to eql(html_return)
    end

    it "com url, cor, ícone e tamanho definidos" do
      html_return = "<a id=\"btn_adicionar_contrato\" class=\"btn btn-primary btn-small\" href=\"url_path\"><i class=\"fa-chart-up\"></i> Adicionar Contrato</a>"
      expect(helper.button_tag(name, url_passed, :primary, "fa-chart-up", "small")).to eql(html_return)
    end

    it "sem url" do
      html_return = "<button class=\"btn btn-default btn-sm\" id=\"btn_adicionar_contrato\" data-disable-with=\"Salvando...\">Adicionar Contrato</button>"
      expect(helper.button_tag(name)).to eql(html_return)
    end

    it "sem url e com cor" do
      html_return = "<button class=\"btn btn-primary btn-sm\" id=\"btn_adicionar_contrato\" data-disable-with=\"Salvando...\">Adicionar Contrato</button>"
      expect(helper.button_tag(name, nil, :primary)).to eql(html_return)
    end

    it "sem url, com cor e ícone" do
      html_return = "<button class=\"btn btn-primary btn-sm\" id=\"btn_adicionar_contrato\" data-disable-with=\"Salvando...\"><i class=\"fa-icon-down-thin\"></i> Adicionar Contrato</button>"
      expect(helper.button_tag(name, nil, :primary, "fa-icon-down-thin")).to eql(html_return)
    end

    it "sem url, com cor, ícone e tamanho" do
      html_return = "<button class=\"btn btn-primary btn-small\" id=\"btn_adicionar_contrato\" data-disable-with=\"Salvando...\"><i class=\"fa-icon-down-thin\"></i> Adicionar Contrato</button>"
      expect(helper.button_tag(name, nil, :primary, "fa-icon-down-thin", "small")).to eql(html_return)
    end
  end

  context "Função delete_button_tag" do
    name        = "Deletar"
    id_name     = "btn_deletar"
    url_passed  = "url_path"
    icone       = "im-remove5"

    it "apenas com url" do
      html_return = "<a id=\"#{id_name}\" class=\"btn btn-danger\" data-confirm=\"null\" rel=\"nofollow\" data-method=\"delete\" href=\"#{url_passed}\"><i class=\"#{icone}\"></i> #{name}</a>"

      expect(helper.delete_button_tag(name, url_passed)).to eql(html_return)
    end

    it "com url, tamanho, icone e texto" do
      html_return = "<a id=\"#{id_name}\" class=\"btn btn-danger btn-small\" data-confirm=\"Vai apagar?\" rel=\"nofollow\" data-method=\"delete\" href=\"#{url_passed}\"><i class=\"fa fa-fw\"></i> #{name}</a>"

      expect(helper.delete_button_tag(name, url_passed, "small", "fa fa-fw", "Vai apagar?")).to eql(html_return)
    end
  end
end
