require 'rails_helper'

describe 'Updating a task' do
  let (:task) { create(:task, :done, name: 'Task 1') }
  let (:list) { create(:list, name: 'List', tasks: [task]) }
  let (:user) { create(:user, email: 'octocat@github.com', password: '1superpassword1', lists: [ list ]) }

  context 'when the user is the task owner' do
    it 'finishing a task' do
      sign_in(user)
      put list_task_path(list, task), done: true, format: :json
      json = json_request_return

      expect_task_is_marked_as_done(user, task.name)
      expect(json['name']).to eq task.name
    end
  end

  context 'when the user is not the task owner' do
    it 'returns unauthorized access' do
      other_task = create(:task, :done, name: 'Other task')
      other_list = create(:list, name: 'Other list', tasks: [other_task])

      sign_in(user)
      put list_task_path(other_list, other_task), done: true, format: :json
      json = json_request_return

      expect_task_is_not_marked_as_done(user, other_task.name)
      expect(json['errors']).to eq ["List not found"]
      expect(response.code).to eq '404'
    end
  end

  # Helpers
  def sign_in(user)
    post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
  end

  def json_request_return
    JSON.parse(response.body)
  end

  def expect_task_is_marked_as_done(user, task_name)
    expect(user.reload.tasks.select { |t| t.name == task_name }.first.done?).to be_truthy
  end

  def expect_task_is_not_marked_as_done(user, task_name)
    expect(user.reload.tasks.include?(task_name)).to be_falsy
  end
end
