require 'rails_helper'

describe 'Favorite lists' do
  let(:user) { create(:user) }
  let(:list) { create(:list) }

  context 'adding a list' do
    it 'saves a list remove favorites' do
      sign_in(user)
      put list_favorite_path(list)

      expect(response.code).to eq '200'
      expect_user_to_have_favorite_list(list)
    end
  end

  context 'removing a list' do
    it 'removes a list from favorites' do
      FavoriteList.add_favorite(user, list)

      sign_in(user)
      put list_unfavorite_path(list)

      expect(response.code).to eq '200'
      expect_user_to_not_have_favorite_list
    end
  end

  context 'try to remove or create favorite in a inexistent list' do
    it 'returns a 404 code' do
      sign_in(user)
      put list_unfavorite_path(9999)
      expect_receive_404_error

      put list_favorite_path(9999)
      expect_receive_404_error
    end
  end

  # Helpers
  def expect_user_to_not_have_favorite_list
    expect(user.reload.favorite_lists).to be_empty
  end

  def expect_user_to_have_favorite_list(list)
    expect(user.reload.favorite_lists.map(&:list_name)).to eq [ list.name ]
  end

  def expect_receive_404_error
    json = JSON.parse(response.body)
    expect(json['errors']).to eq ["List not found"]
    expect(response.code).to eq '404'
  end

  def sign_in(user)
    post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
  end
end
