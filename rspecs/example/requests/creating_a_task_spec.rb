require 'rails_helper'

describe 'Task creation' do
  let (:list) { create(:list, name: 'New list') }
  let (:user) { create(:user, email: 'octocat@github.com', password: '1superpassword1', lists: [ list ]) }

  context 'with name' do
    it 'creates a new task and returns a json' do
      task_name = 'New Task'

      sign_in(user)
      post list_tasks_path(list), name: task_name, format: :json
      json = json_request_return

      expect_user_to_have_task(task_name)
      expect(json['name']).to eq task_name
      expect(json['url']).to eq list_task_path(list, list.tasks.first.id)
    end
  end

  context 'without name' do
    it 'does not save' do
      task_name = ''

      sign_in(user)
      post list_tasks_path(list), name: task_name, format: :json
      json = json_request_return

      expect(json['errors']).to eq ["Name can't be blank"]
      expect(response.code).to eq '422'
    end
  end

  context 'security verification' do
    it 'returns 404 error code when a user is not the list owner' do
      task_name = 'New Task'
      other_list = create(:list, name: 'New list from other user')

      sign_in(user)
      post list_tasks_path(other_list), name: task_name, format: :json
      json = json_request_return

      expect(json['errors']).to eq ["List not found"]
      expect(response.code).to eq '404'
    end
  end

  # Helpers
  def sign_in(user)
    post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
  end

  def json_request_return
    JSON.parse(response.body)
  end

  def expect_user_to_have_task(task_name)
    expect(list.reload.tasks.pluck(:name)).to eq [ task_name ]
  end
end
