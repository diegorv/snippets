require 'rails_helper'

feature 'User signup' do
  before do
    visit(root_path)
    should_be_in_url_path(new_user_session_path)
    click_link('Sign up')
  end

  scenario 'with success' do
    fill_in_sign_up_form
    should_have_link('Sign out')
  end

  context 'with errors' do
    scenario 'invalid email' do
      fill_in_sign_up_form(email: 'x@x')
      email_field_should_have_error('is invalid')
    end

    scenario 'empty email' do
      fill_in_sign_up_form(email: '')
      email_field_should_have_error('can\'t be blank')
    end

    scenario 'empty password' do
      fill_in_sign_up_form(password: '')
      password_field_should_have_error('can\'t be blank')
    end
  end

  # Helpers
  def fill_in_sign_up_form(options = {email: 'octocat@github.com', password: '1superpassword1'})
    fill_in 'Email'                , with: options[:email]
    fill_in 'Password'             , with: options[:password]
    fill_in 'Password confirmation', with: options[:password]
    click_button 'Sign up'
  end

  def email_field_should_have_error msg
    within '.user_email' do
      should_see(msg)
    end
  end

  def password_field_should_have_error msg
    within '.user_password' do
      should_see(msg)
    end
  end
end
