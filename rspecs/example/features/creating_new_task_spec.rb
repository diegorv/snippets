require 'rails_helper'

feature 'Creating new task' do
  let (:list) { create(:list, name: 'List name') }
  let (:user) { create(:user, lists: [ list ]) }

  scenario 'with success', js: true do
    task_name = 'New task'
    login_user_without_form(user)
    click_link(list.name)
    create_task(task_name)
    expect_task_as_created_with_success(user, task_name)
  end

  scenario 'without a name', js: true do
    login_user_without_form(user)
    click_link(list.name)
    create_task
    expect_task_was_not_created_with_errors(list)
  end

  # Helpers
  def create_task(task_name = nil)
    element = find('.list_tasks_name>input')
    element.set(task_name) if !task_name.nil?
    element.native.send_key(:Enter)
    wait_for_ajax
  end

  def expect_task_as_created_with_success(user, task_name)
    expect(page).to have_css(".task", text: task_name)
    expect(find('.task>input[type="checkbox"]')).to be_truthy
    expect(find('.list_tasks_name>input').value).to be_empty
    expect(user.reload.tasks.map(&:name)).to eq [ task_name ]
  end

  def expect_task_was_not_created_with_errors(list)
    expect(page).not_to have_css('.task')
    expect(page).to have_css('.error', text: "Name can't be blank")
    expect(list.reload.tasks.count).to eq 0
  end
end
