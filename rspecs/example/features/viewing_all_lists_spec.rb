require 'rails_helper'

feature 'Viewing all lists' do
  let (:user) { create(:user, lists: [create(:list), create(:list)]) }

  before do
    login_user_without_form(user)
  end

  scenario 'Private lists' do
    click_on('My task lists')
    should_be_in_url_path(personal_lists_path)
    have_user_lists
  end

  scenario 'Public lists' do
    lists = [
      create(:list, :public),
      create(:list, :public)
    ]

    click_on('Public lists')
    should_be_in_url_path(public_lists_path)
    expect_to_have_public_lists(lists)
  end

  scenario 'Favorite lists' do
    list = create(:list, :public)
    FavoriteList.add_favorite(user, list)

    click_on('Favorite lists')
    should_be_in_url_path(favorites_lists_path)
    should_see(list.name)
  end

  # Helpers
  def expect_user_to_have_lists
    user.lists.each do |list|
      expect(page).to have_css('.list', text: list.name)
    end
  end

  def expect_to_have_public_lists(lists)
    lists.each do |list|
      expect(page).to have_css('.list', text: list.name)
    end
  end
end
