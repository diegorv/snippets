require 'rails_helper'

feature 'Creating todo list' do
  let (:user)      { create(:user)   }
  let (:list_name) { 'New todo list' }

  before do
    login_user_without_form(user)
  end

  scenario 'with success' do
    click_link    'New list'
    fill_in       'List name', with: list_name
    click_button  'Create'

    list = reload_user_list

    expect_title_to_be(list_name)
    expect_user_to_have_list(list_name)
    expect_list_to_not_be_public(list)
  end

  scenario 'public list' do
    click_link    'New list'
    fill_in       'List name', with: list_name
    check         'Make it public!'
    click_button  'Create'

    list = reload_user_list
    expect_list_to_be_public(list)
  end

  scenario 'with empty name' do
    click_link    'New list'
    click_button  'Create'
    should_see    'can\'t be blank'
  end

  # Helpers
  def reload_user_list
    user.reload.lists.find_by(name: list_name)
  end

  def expect_user_to_have_list(list_name)
    expect(user.reload.lists.pluck(:name)).to eq [ list_name ]
  end

  def expect_list_to_not_be_public(list)
    expect(list.public_list?).to be_falsy
  end

  def expect_list_to_be_public(list)
    expect(list.public_list?).to be_truthy
  end

  def expect_title_to_be(name)
    expect(page).to have_css('h1', text: list_name)
  end
end
