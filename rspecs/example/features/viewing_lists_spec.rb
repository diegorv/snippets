require 'rails_helper'

feature 'Viewing lists' do
  let (:list) { create(:list) }
  let (:user) { create(:user, lists: [ list ]) }

  scenario 'signed in user can only see own lists' do
    other_list = create(:list, name: 'List without user')

    login_user_without_form(user)
    expect_to_have_list(list.name)
    expect_to_not_have_list(other_list.name)
  end

  scenario 'signed in user can see public lists' do
    other_list = create(:list, :public, name: 'List without user')

    login_user_without_form(user)
    expect_to_have_list(list.name)
    expect_to_have_a_public_list(other_list.name)
  end

  scenario 'redirect to home when a user try see a private list from other user' do
    other_list = create(:list, name: 'Private List')
    other_user = create(:user, lists: [ other_list ])

    login_user_without_form(user)

    visit(list_path(other_list))
    should_be_in_url_path(root_path)
  end

  # Helpers
  def expect_to_have_list(list_name)
    expect(page).to have_css('.list', text: list_name)
  end

  def expect_to_not_have_list(list_name)
    expect(page).to_not have_css('.list', text: list_name)
  end

  def expect_to_have_a_public_list(list_name)
    expect(page).to have_css('.public', text: list_name)
  end
end
