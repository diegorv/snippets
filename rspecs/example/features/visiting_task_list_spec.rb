require 'rails_helper'

feature 'Visiting a task list' do
  let (:list)  { create(:list, name: 'List', tasks: [create(:task, :done, name: 'Task 1')]) }
  let (:user)  { create(:user, lists: [ list ]) }

  scenario 'when there are finished tasks' do
    visit_list(user, list)
    expect_task_checkbox_is_checked
  end

  context 'task checkboxes condition' do
    scenario 'have to not be disabled when a user visits your own list' do
      visit_list(user, list)
      expect_task_checkbox_to_not_be_disabled
    end

    scenario 'have to be disabled when a user visits a list from other user' do
      public_list = create(:list, name: 'List', tasks: [create(:task, :done, name: 'Task 1')], public_list: true)
      public_user = create(:user, lists: [ public_list ])

      visit_list(user, public_list)
      expect_task_checkbox_to_be_disabled
    end
  end

  # Helpers
  def visit_list(user, list)
    login_user_without_form(user)
    visit(list_path(list))
  end

  def expect_task_checkbox_is_checked
    within '.tasks' do
      expect(page).to have_css("input[type='checkbox'][checked]")
    end
  end

  def expect_task_checkbox_to_be_disabled
    within '.tasks' do
      expect(page).to have_css("input[type='checkbox'][disabled]")
    end
  end

  def expect_task_checkbox_to_not_be_disabled
    within '.tasks' do
      expect(page).to_not have_css("input[type='checkbox'][disabled]")
    end
  end
end
