require 'rails_helper'

feature 'Favorite lists' do
  let(:user) { create(:user) }

  scenario 'user removes a list from favorites', js: true do
    list = create(:list, :public, name: 'List 14')
    FavoriteList.add_favorite(user, list)

    login_user_without_form(user)
    remove_list_from_favorites
    visit(root_path)

    expect_page_to_not_have_favorites
    expect_user_to_not_have_favorites(user)
  end

  scenario 'user favorites a list', js: true do
    list = create(:list, :public, name: 'List 13')

    login_user_without_form(user)
    favorites_a_list
    visit(root_path)

    expect_page_to_have_favorites(list)
    expect_user_to_have_favorites(list)
  end

  # Helpers
  def expect_page_to_not_have_favorites
    expect(page).not_to have_css('.favorite')
  end

  def expect_user_to_not_have_favorites(user)
    expect(user.reload.favorite_lists).to be_empty
  end

  def remove_list_from_favorites
    find('.list>a', match: :first).click
    uncheck('favorite')
    wait_for_ajax
  end

  def favorites_a_list
    find('.list>a', match: :first).click
    check('favorite')
    wait_for_ajax
  end

  def expect_page_to_have_favorites(list)
    expect(page.find('.favorite')).to have_link(list.name)
  end

  def expect_user_to_have_favorites(list)
    expect(user.reload.favorite_lists.map(&:list_name)).to eq [ list.name ]
  end
end
