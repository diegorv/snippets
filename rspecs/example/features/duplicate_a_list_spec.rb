require 'rails_helper'

feature 'Duplicate lists' do
  let(:user) { create(:user) }

  context 'public list' do
    scenario 'user can duplicate a list with taks and make it private' do
      tasks       = [create(:task, :done, name: 'Task 1'), create(:task, name: 'Task 2')]
      public_list = create(:list, name: 'Public List', public_list: true, tasks: tasks)
      other_user  = create(:user, lists: [ public_list ])

      login_user_without_form(user)
      click_link(public_list.name)
      click_link('Copy and make this list private')
      should_see(public_list.name)
      expect_user_to_have_duplicated_list_with_same_tasks(public_list)
    end
  end

  context 'private list' do
    let(:private_list) { create(:list, name: 'Private List') }

    before do
      user.lists << private_list
      login_user_without_form(user)
    end

    scenario 'user can not be able to duplicate it' do
      click_link(private_list.name)
      should_not_see('Copy and make this list private')
    end

    scenario 'try duplicate a list directly accessing the url' do
      force_duplicate_list_by_url_path(private_list)
      should_be_in_url_path(root_path)
    end
  end

  # Helpers
  def expect_user_to_have_duplicated_list_with_same_tasks(other_list)
    expect(user.reload.lists.first.tasks.map(&:name)).to eq other_list.tasks.map(&:name)
  end

  def force_duplicate_list_by_url_path(list)
    visit(duplicate_and_make_private_list_path(list))
  end
end
