require 'rails_helper'

feature 'User session' do
  let (:user) { create(:user) }

  scenario 'user wants to sign in' do
    sign_in(user)
    expect(page).to have_link 'Sign out'
  end

  scenario 'signed in user wants to sign out' do
    sign_in(user)
    click_link('Sign out')
    expect(page).to have_content('Log in')
  end

  scenario 'unsigned user' do
    visit(root_path)
    expect(page).not_to have_link('Sign out')
  end
end
