require 'rails_helper'

feature 'Updating a task' do
  scenario 'finishing a task', js: true do
    task_name = 'New task'
    list      = create(:list, name: 'List')
    user      = create(:user, lists: [ list ])

    login_user_without_form(user)

    click_link(list.name)
    create_new_task(task_name)
    expect_task_is_marked_as_done(user, task_name)
  end

  scenario 'when a task is marked as done and a user removes the check', js: true do
    task_name = 'Task 1'
    task      = create(:task, :done, name: task_name)
    list      = create(:list, name: 'List', tasks: [ task ])
    user      = create(:user, lists: [ list ])

    login_user_without_form(user)

    click_link(list.name)
    uncheck_task(task)
    expect_task_is_not_marked_as_done(user, task_name)
  end

  # Helpers
  def create_new_task(task_name)
    element = find('.list_tasks_name>input')
    element.set(task_name)
    element.native.send_key(:Enter)
    find('.task>input').click
    wait_for_ajax
  end

  def uncheck_task(task)
    uncheck task.name
    wait_for_ajax
  end

  def expect_task_is_marked_as_done(user, task_name)
    expect(page).to have_css('.done', text: (task_name))
    expect(user.reload.tasks.select { |t| t.name == task_name }.first.done?).to be_truthy
  end

  def expect_task_is_not_marked_as_done(user, task_name)
    expect(page).not_to have_css('.done')
    expect(user.reload.tasks.select { |t| t.name == task_name }.first.done?).to be_falsy
  end
end
