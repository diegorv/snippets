# encoding: utf-8
require 'rails_helper'

describe "Manage Clients" do
	before do
		create_user_logged(role: "Atendimento")
		create_default_city
		visit root_path
		click_link "Relacionamento"
		click_link "Gerenciamento de clientes"
		should_see "Gerenciamento de clientes"
		click_link  "Adicionar novo cliente"
		should_see  "Adicionar cliente"
	end

	it "com um endereço", js: true do
		fill_basic_client_values
		create_address_steps
		click_button	"Salvar"
		should_see 		"Cliente foi criado com sucesso."
		check_basic_client_values
		check_address_steps
	end

	it "com um meio de contato", js: true do
		fill_basic_client_values
		create_address_steps
		create_contact_steps
		click_button	"Salvar"
		should_see 		"Cliente foi criado com sucesso."
		check_basic_client_values
		check_address_steps
		check_contact_steps
	end

	it "com um funcionario", js: true do
		fill_basic_client_values
		create_address_steps
		create_employee_steps
		click_button 	"Salvar"
		should_see 		"Cliente foi criado com sucesso."
		check_basic_client_values
		check_address_steps
		check_employee_steps
	end

	it "com todos os dados", js: true do
		fill_basic_client_values
		create_address_steps
		create_contact_steps
		create_employee_steps
		click_button	"Salvar"
		should_see 		"Cliente foi criado com sucesso."
		check_basic_client_values
		check_address_steps
		check_contact_steps
		check_employee_steps
	end
end

# -------------------------------------------------------------------------------------

def fill_basic_client_values
	select      "Pessoa juridica" 						, from: "Tipo"
	select      "Especial"        						, from: "Prioridade"
	fill_in_js 	"client_client_document_head"	, with: "96.249.754/0001-59"
	fill_the_following(
		"Inscrição Estadual/Municipal"  => "9999999999",
		"Razão Social"        => "Editora Dominio e Distribuidora LTDA",
		"Nome fantasia"       => "Editora Dominio"
	)
end

def check_basic_client_values
	fields_should_contain_the_following(
		"Tipo" 							=> "Pessoa juridica",
		"Prioridade"    		=> "Especial",
		"CNPJ"							=> "96.249.754/0001-59",
		"Inscrição Estadual/Municipal"=> "9999999999"
	)
	click_link "1. Nome da empresa"
	fields_should_contain_the_following(
		"Razão Social"        => "Editora Dominio e Distribuidora LTDA",
		"Nome fantasia"       => "Editora Dominio"
	)
end

def create_address_steps
	click_link    "2. Endereços"
	click_button  "Adicionar endereço"
	select        "Diagramação" , from: "Tipo de endereço"
	check         "Internet?"
	check         "Diagramar?"
	select        "Brasil"      , from: "País"
	check         "Padrão"
	fill_in       "Cep"         , with: "02035020"
	select        "São Paulo"   , from: "Estado"
	select        "São Paulo"   , from: "Cidade"
	field_should_contain "Logradouro"	, "Rua Doutor Zuquim"
	field_should_contain "Bairro"			, "Santana"
	fill_the_following(
		"Número"        => "576",
		"Complemento"   => "Empresa",
		"Caixa Postal"  => "011",
		"Observação"    => "Observação"
	)
end

def check_address_steps
	click_link "2. Endereços"
	 fields_should_contain_the_following(
		"Tipo de endereço" 	=> "Diagramação",
		"Internet?"			    => "Sim",
		"Diagramar?"		    => "Sim",
		"Logradouro"  			=> "Rua Doutor Zuquim",
		"Número"        		=> "576",
		"Complemento"   		=> "Empresa",
		"Bairro"						=> "Santana",
		"Local"					  	=> "São Paulo - SP - Brasil",
		"Cep"								=> "02035020",
		"Caixa Postal"  		=> "011",
		"Observação"    		=> "Observação"
	)
end

def create_contact_steps
	click_link    "3. Meios de contato"
	click_button  "Adicionar meio de contato"
	select        "Celular"  , from: "Tipo de contato"
	fill_in       "Celular"  , with: "11996164998"
  check         "Internet?"
  check         "Diagramar?"
end

def check_contact_steps
	click_link "3. Meios de contato"
  within(".contatos_field") do
  	fields_should_contain_the_following(
  		"Tipo" 							=> "Celular",
  		"Celular"    				=> "(11) 99616-4998",
  		"Internet?"	        => "Sim",
  		"Diagramar?"	      => "Sim"
  	)
  end
end

def create_employee_steps
	click_link    "4. Funcionários"
	click_button  "Adicionar funcionário"
	select        "Sócio"       , from: "Tipo funcionário"
	fill_in       "Nome"        , with: "Diego"
	fill_in       "Aniversário" , with: "15/04/1988"
  check         "Internet?"
  check         "Diagramar?"
end

def check_employee_steps
	click_link "4. Funcionário"
  within(".funcionarios_field") do
  	fields_should_contain_the_following(
  		"Cargo" 						=> "Sócio",
  		"Nome"    					=> "Diego",
  		"Aniversário"				=> "15/04/1988",
  		"Diagramar?"		    => "Sim",
  		"Internet?"		      => "Sim"
  	)
  end
end
