require 'rspec'
require 'active_record'
require './app/presenters/list_collection_presenter'
require './app/models/list'

describe ListCollectionPresenter do
  describe '.for' do
    it 'returns an instance of ListCollectionPresenter' do
      user = double
      list_collection_presenter = ListCollectionPresenter.for(user)

      expect(list_collection_presenter).to be_an_instance_of(ListCollectionPresenter)
    end
  end

  describe '#personal_lists' do
    it 'delegates to user' do
      user = spy
      list_collection_presenter = ListCollectionPresenter.new(user)
      list_collection_presenter.personal_lists

      expect(user).to have_received(:lists)
    end
  end

  describe '#recent_public_lists' do
    it 'delegates to user' do
      user = double
      allow(List).to receive(:recent_public_list)
      list_collection_presenter = ListCollectionPresenter.new(user)
      list_collection_presenter.recent_public_lists

      expect(List).to have_received(:recent_public_list)
    end
  end

  describe '#favorite_lists' do
    it 'returns favorite lists of user' do
      user = spy
      list_collection_presenter = ListCollectionPresenter.new(user)
      list_collection_presenter.favorite_lists

      expect(user).to have_received(:favorite_lists)
    end
  end
end
