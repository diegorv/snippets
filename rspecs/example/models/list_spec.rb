require 'rails_helper'

describe List do
  context 'associations' do
    it { should have_many(:tasks) }
  end

  context 'creating a record without name' do
    it { should validate_presence_of(:name) }
  end

  describe '.recent_public_list' do
    it 'returns a collection of recent public list' do
      create(:list, name: 'first public', public_list: true)
      create(:list, name: 'private')
      create(:list, name: 'second public', public_list: true)

      expect(List.recent_public_list.map(&:name)).to eq [ 'second public', 'first public' ]
    end

    it 'returns a collection of public lists ordered by created_at' do
      6.times do |index|
        index += 1
        create(:list, name: index, public_list: true)
      end

      expect(List.recent_public_list.map(&:name)).to eq [ '6', '5', '4', '3', '2' ]
    end
  end

  describe '.all_public_lists' do
    it 'returns all public lists' do
      public_list = create(:list, :public)
      create(:list)

      expect(List.all_public_lists.map(&:name)).to eq [ public_list.name ]
    end
  end

  describe '#created_by?' do
    context 'when a list is created by a given user' do
      it 'returns true' do
        list = create(:list)
        user = create(:user, lists: [ list ])

        expect(list.created_by?(user)).to be_truthy
      end
    end

    context 'when a list is not created by a given user' do
      it 'returns false' do
        list = create(:list)
        user = create(:user)

        expect(list.created_by?(user)).to be_falsy
      end
    end
  end

  describe '#duplicate_and_make_private_to' do
    it 'create a new private list with the same tasks and set to a new user' do
      old_task = [create(:task, :done, name: 'Task 1'), create(:task, name: 'Task 2')]
      old_list = create(:list, name: 'List', public_list: true, tasks: old_task)
      old_user = create(:user, lists: [ old_list ])
      new_user = create(:user)

      new_list = old_list.duplicate_and_make_private_to(new_user)

      expect(new_list.name).to eq old_list.name
      expect(new_list.user_id).to eq new_user.id
      expect(new_list.public_list).to be_falsy
      expect(new_list.tasks.map(&:name)).to eq old_list.tasks.map(&:name)
    end
  end
end
