#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::Contract, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:client).class_name("Client::Client")                             }
    it { should belong_to(:client_company).class_name("Client::Company")                    }
    it { should belong_to(:representative).class_name("User").conditions(occupation_id: 11) }
    it { should belong_to(:telemarketing).class_name("User").conditions(occupation_id: 12)  }
    it { should have_many(:items)                                                           }
    it { should have_many(:payments)                                               }
  end

  # -------------------------------------------------------------------------------------

  context "nested" do
    it { should accept_nested_attributes_for(:items).allow_destroy(true) }
    it { should accept_nested_attributes_for(:payments).allow_destroy(true) }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
    it { should validate_presence_of(:representative_id)    }
    it { should validate_presence_of(:number)               }
    it { should validate_presence_of(:client_company_id)    }
    it { should validate_presence_of(:signature_date)       }
    it { should validate_presence_of(:price)                }
    it { should validate_presence_of(:parcels_number)       }
    it { should validate_presence_of(:first_parcel_date)    }
  end

  # -------------------------------------------------------------------------------------
end
