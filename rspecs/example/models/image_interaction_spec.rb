#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::ImageInteraction, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:image)  }
    it { should belong_to(:user)   }
  end

  # -------------------------------------------------------------------------------------
end
