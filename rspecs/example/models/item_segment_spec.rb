#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::ItemSegment, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:item)                                    }
    it { should belong_to(:segment).class_name("Segment::Segment")  }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
    it { should validate_presence_of(:segment_id) }
  end

  # -------------------------------------------------------------------------------------
end
