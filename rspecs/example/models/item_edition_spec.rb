#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::ItemEdition, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:item)                                            }
    it { should belong_to(:product_edition).class_name("Product::Edition")  }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
   it { should validate_presence_of(:product_edition_id)    }
  end

  # -------------------------------------------------------------------------------------
end
