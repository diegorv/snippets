require 'rails_helper'

describe Contract::Payment, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:contract) }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
    it { should validate_presence_of(:payable_type) }
    it { should validate_presence_of(:parcel_date)   }
  end

  # -------------------------------------------------------------------------------------
end
