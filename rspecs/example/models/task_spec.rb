require 'rails_helper'

describe Task do
  context 'associations' do
    it { should belong_to(:list) }
  end

  context 'creating a record without name' do
    it { should validate_presence_of(:name) }
  end

  describe '#done?' do
    context 'when a task is done' do
      it 'returns true' do
        task = Task.new(name: 'Task', done: 1)

        expect(task.done?).to be_truthy
      end
    end

    context 'when a task is not done' do
      it 'returns false' do
        task = Task.new(name: 'Task', done: 0)

        expect(task.done?).to be_falsy
      end
    end
  end

  describe '.recent_tasks' do
    it 'returns a collection of latest tasks' do
      20.times do |index|
        index += 1
        create(:task, name: index)
      end

      expect(Task.recent_tasks.map(&:name)).to eq ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    end
  end
end
