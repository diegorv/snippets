#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::Image, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:item)                                                       }
    it { should belong_to(:product).class_name("Product::Product")                     }
    it { should belong_to(:product_specification).class_name("Product::Specification") }
    it { should belong_to(:user)                                                       }
    it { should have_many(:image_interactions)                                          }
  end

  # -------------------------------------------------------------------------------------
end
