require "simple_test_helper"

describe User do
  context 'validations' do
    should validate_presence_of(:name)
  end

  context 'valid data' do
    it "precisa ser válido" do
      user      = create(:user)
      user.name = "Diego"
      assert user.valid?
    end

    it "precisa ter nome diferente de Rafael" do
      user      = create(:user)
      user.name.wont_match "Rafael"
    end
  end
end
