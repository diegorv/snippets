#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::Item, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:contract)                                                    }
    it { should belong_to(:product).class_name("Product::Product")                      }
    it { should belong_to(:product_specification).class_name("Product::Specification")  }
    it { should have_many(:item_cities)                                                 }
    it { should have_many(:item_editions)                                               }
    it { should have_many(:item_segments)                                               }
    it { should have_one(:image)                                                        }
  end

  # -------------------------------------------------------------------------------------

  context "nested" do
    it { should accept_nested_attributes_for(:item_cities).allow_destroy(true)   }
    it { should accept_nested_attributes_for(:item_editions).allow_destroy(true) }
    it { should accept_nested_attributes_for(:item_segments).allow_destroy(true) }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
    it { should validate_presence_of(:product_id)               }
    it { should validate_presence_of(:product_specification_id) }
    # @TODO: Testar presença da cidade e segmento
  end

  # -------------------------------------------------------------------------------------
end
