require 'rails_helper'

describe FavoriteList do
  context 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:list) }
  end

  describe '#add_favorite' do
    it 'adds lists to favorite lists' do
      lists = [
        create(:list, name: 'List 1'),
        create(:list, name: 'List 2')
      ]
      user = create(:user)
      lists.each do |list|
        FavoriteList.add_favorite(user, list)
      end
      user.save

      expect(user.reload.favorite_lists.map(&:list_name)).to eq [ 'List 1', 'List 2' ]
    end

    it 'can not add a list twice for a user' do
      list = create(:list, name: 'List 2')
      user = create(:user)

      2.times do
        FavoriteList.add_favorite(user, list)
      end

      user.save
      expect(user.reload.favorite_lists.map(&:list_name)).to eq [ 'List 2' ]
    end
  end

  describe '.remove_favorite' do
    it 'removes given list from favorites' do
      list = create(:list, name: 'List 2')
      user = create(:user)
      FavoriteList.add_favorite(user, list)
      FavoriteList.remove_favorite(user, list)

      expect(user.reload.favorite_lists.map(&:list_name)).not_to include(list.name)
    end
  end
end
