require 'rails_helper'

describe User do
  context 'associations' do
    it { should have_many(:lists) }
  end

  describe '#favorited?' do
    it 'returns true' do
      list = create(:list, name: 'List 2')
      user = create(:user)
      FavoriteList.add_favorite(user, list)

      expect(user.reload.favorited?(list)).to be_truthy
    end
  end
end
