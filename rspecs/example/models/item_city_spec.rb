#
# Namespace : Contract
# Date      : 05/03/15 - 4:10PM
# Version   : 0.1
# Status    : Integridade verificada
#
require 'spec_helper'

describe Contract::ItemCity, :type => :model do
  # -------------------------------------------------------------------------------------

  context "associations" do
    it { should belong_to(:item)                                    }
    it { should belong_to(:country).class_name("Address::Country")  }
    it { should belong_to(:state).class_name("Address::State")      }
    it { should belong_to(:city).class_name("Address::City")        }
  end

  # -------------------------------------------------------------------------------------

  context "validations" do
    it { should validate_presence_of(:country_id) }
    it { should validate_presence_of(:state_id)   }
    it { should validate_presence_of(:city_id)    }
  end

  # -------------------------------------------------------------------------------------
end
