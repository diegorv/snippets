FactoryGirl.define do
  factory :list do
    name 'New list'
    public_list false

    trait :public do
      public_list true
    end
  end
end
