FactoryGirl.define do
  factory :country, :class => Address::Country do
    sequence(:name) {|n| "#{Forgery::Name.first_name}#{n}" }
  end

  factory :city, :class => Address::City do
    state
    sequence(:name) {|n| "#{Forgery::Name.first_name}#{n}" }
  end

  factory :state, :class => Address::State do
    country
    uf              { Forgery::Name.first_name }
    sequence(:name) {|n| "#{Forgery::Name.first_name}#{n}" }
  end

  factory :neighborhood, :class => Address::Neighborhood do
    city
    sequence(:name) {|n| "#{Forgery::Name.first_name}#{n}" }
  end

  factory :cep, :class => Address::Cep do
    zipcode       { "02035020" }
    state
    city
    neighborhood
    street_type   { "Rua" }
    street_name   { "Doutor Zuquim" }
  end
end
