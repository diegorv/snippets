ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require "minitest/rails/capybara"

require "minitest/reporters"
Minitest::Reporters.use!(Minitest::Reporters::SpecReporter.new, ENV, Minitest.backtrace_filter)

require 'capybara/poltergeist'

Capybara.default_driver     = :rack_test
Capybara.javascript_driver  = :poltergeist

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  before :each do
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
    Capybara.reset_sessions!
  end
end
