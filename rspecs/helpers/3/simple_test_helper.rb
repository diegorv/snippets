ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

require "minitest/rails/capybara"

require "minitest/reporters"
Minitest::Reporters.use!(Minitest::Reporters::SpecReporter.new, ENV, Minitest.backtrace_filter)

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!
  include FactoryGirl::Syntax::Methods

  before :each do
    DatabaseCleaner.strategy  = :transaction
    DatabaseCleaner.start
  end

  after :each do
    DatabaseCleaner.clean
  end
end
