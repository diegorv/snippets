ENV["RAILS_ENV"] ||= 'test'

if ENV["CODECLIMATE_REPO_TOKEN"]
  require "codeclimate-test-reporter"
  CodeClimate::TestReporter.start
end

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'

# ------------------------------------------------------------------------------------- #

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# ------------------------------------------------------------------------------------- #

# Checks for pending migrations before tests are run.
ActiveRecord::Migration.maintain_test_schema!

# ------------------------------------------------------------------------------------- #

RSpec.configure do |config|
  # config.mock_with :mocha
  config.fixture_path                               = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures                 = true
  config.infer_base_class_for_anonymous_controllers = false

  config.infer_spec_type_from_file_location!

  config.include FactoryGirl::Syntax::Methods

  config.before(:each) do
    PaperTrail.enabled        = false
    DatabaseCleaner.strategy  = :transaction
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

# -------------------------------------------------------------------------------------

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end
