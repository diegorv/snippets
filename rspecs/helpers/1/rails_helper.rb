ENV["RAILS_ENV"] ||= 'test'

if ENV["CODECLIMATE_REPO_TOKEN"]
  require "codeclimate-test-reporter"
  CodeClimate::TestReporter.start
end

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'

require 'capybara/rails'
require 'capybara/rspec'
require 'database_cleaner'

# ------------------------------------------------------------------------------------- #

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# ------------------------------------------------------------------------------------- #

# Checks for pending migrations before tests are run.
ActiveRecord::Migration.maintain_test_schema!

# ------------------------------------------------------------------------------------- #

class WebkitStderrWithQtPluginMessagesSuppressed
  IGNOREABLE = Regexp.new( [
    'CoreText performance',
    'userSpaceScaleFactor',
    'Internet Plug-Ins',
    'is implemented in bo'
  ].join('|') )

  def write(message)
    if message =~ IGNOREABLE
      0
    else
      puts(message)
      1
    end
  end
end

Capybara.register_driver :webkit_with_qt_plugin_messages_suppressed do |app|
  Capybara::Webkit::Driver.new(
    app,
    Capybara::Webkit::Configuration.to_hash.merge(  # <------ maintain configuration set in Capybara::Webkit.configure block
      stderr: WebkitStderrWithQtPluginMessagesSuppressed.new
    )
  )
end

Capybara.javascript_driver = :webkit_with_qt_plugin_messages_suppressed

=begin
require 'selenium-webdriver'

Capybara.javascript_driver = :selenium
Selenium::WebDriver::Firefox::Binary.path='/Applications/Firefox.app/Contents/MacOS/firefox-bin'

module ::Selenium::WebDriver::Remote
   class Bridge
     def execute(*args)
       res = raw_execute(*args)['value']
       sleep 0.3
       res
     end
   end
 end

Capybara.server do |app, port|
  require 'rack/handler/webrick'
  Rack::Handler::WEBrick.run(app, :Port => port, :AccessLog => [], :Logger => WEBrick::Log::new(Rails.root.join("log/capybara_test.log").to_s))
end
=end

# ------------------------------------------------------------------------------------- #

class ActiveRecord::Base
  mattr_accessor :shared_connection
  @@shared_connection = nil

  def self.connection
    @@shared_connection || ConnectionPool::Wrapper.new(:size => 1) { retrieve_connection }
  end
end

ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

include Warden::Test::Helpers

# ------------------------------------------------------------------------------------- #

Capybara::Webkit.configure do |config|
  config.block_unknown_urls
end

RSpec.configure do |config|
  #config.raise_errors_for_deprecations!
  config.fixture_path                               = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures                 = false
  config.infer_base_class_for_anonymous_controllers = false
  config.expose_current_running_example_as          :example

  config.include FactoryGirl::Syntax::Methods
  config.infer_spec_type_from_file_location!

  config.before(:each) do
    PaperTrail.enabled = false
    DatabaseCleaner.start
  end

  config.after(:each)  do
    DatabaseCleaner.clean
    Capybara.reset_sessions!
  end
end

# ------------------------------------------------------------------------------------- #
