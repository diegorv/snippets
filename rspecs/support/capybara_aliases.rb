def should_see(text)
  expect(page).to have_content(text)
end

def should_not_see(text)
  expect(page).not_to have_content(text)
end

def should_see_the_following(*contents)
  contents.each do |content|
    expect(page).to have_content(content)
  end
end

def fill_the_following(fields={})
  fields.each do |field, value|
    fill_in field,  :with => value
  end
end

def fields_should_contain_the_following(fields={})
  fields.each do |field, value|
    field = find_by_id(field)

    field_value = (field.tag_name == 'textarea') ? field.text : field.value
    if field_value.respond_to? :should
      expect(field_value).to eq value
    else
      assert_match(/#{value}/, field_value)
    end
  end
  #find_by_id(field).value.should eq value
  #find_field(field).value.should eq value
end

def click_td(linha, text)
  case linha
    when "Primeira"
      @pos = 1
    when "Segunda"
      @pos = 2
    when "Terceira"
      @pos = 3
  end

  within("table tbody tr:nth-child(#{@pos})") do
     click_link text
   end
end

def field_should_contain(field, value)
  field = find_field(field)
  field_value = (field.tag_name == 'textarea') ? field.text : field.value
  if field_value.respond_to? :should
    expect(field_value).to eq value
  else
    assert_match(/#{value}/, field_value)
  end

  #assert_match(/#{value}/, page.body)
  #find_field(field).value.should eq value
end

def field_by_id_should_contain(field, options = {})
  field = find_by_id(field)
  field_value = (field.tag_name == 'textarea') ? field.text : field.value
  if field_value.respond_to? :should
    expect(field_value).to eq options[:with]
  else
    assert_match(/#{options[:with]}/, field_value)
  end

  #assert_match(/#{value}/, page.body)
  #find_field(field).value.should eq value
end

def select_date(date, field)
  date        = Date.parse(date)
  base_dom_id = find(:xpath, ".//label[contains(., '#{field}')]")['for'].gsub(/(_[1-5]i)$/, '')
  if date.year != 0
    find(:xpath, ".//select[@id='#{base_dom_id}_1i']").select(date.year.to_s)
  end
  find(:xpath, ".//select[@id='#{base_dom_id}_2i']").select(I18n.l date, :format => '%B')
  find(:xpath, ".//select[@id='#{base_dom_id}_3i']").select(date.day.to_s)
end

def reload_page
  current_path = URI.parse(current_url).path
  visit("#{current_path}?as")
end
