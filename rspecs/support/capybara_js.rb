def submit_form(element)
  page.driver.browser.execute_script("$('##{element}').submit();")
  #page.execute_script("$('##{element}').submit();") CAPYBARA 2.0
end

def run_browser_javascript
  #page.driver.browser.execute_script %Q{#{yield}}
  page.execute_script %Q{#{yield}}
end

def fill_in_js (field_name, options = {})
  sleep(0.1)
  page.execute_script %{ $("##{field_name}").val("#{options[:with]}"); }
end

def select_autocomplete(abreviado, valor, within_name)
  within within_name do
    find("#{within_name} .chosen-container-single").click
    sleep(0.1)
    page.execute_script %{ $("#{within_name} .chosen-search input").val("#{abreviado}"); }
    find('li', text: valor).click
  end
end

def accept_alert_dialog
  page.evaluate_script 'window.original_confirm_function = window.confirm;'
  page.evaluate_script 'window.confirm = function(msg) { return true; }'
  page.evaluate_script 'window.confirm = window.original_confirm_function;'
end

def dismiss_alert_dialog
  page.evaluate_script 'window.original_confirm_function = window.confirm;'
  page.evaluate_script 'window.confirm = function(msg) { return false; }'
  page.evaluate_script 'window.confirm = window.original_confirm_function;'
end
