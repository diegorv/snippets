# encoding: utf-8
# -------------------------------------------------------------------------------------

def create_default_user(options = {})
  @user = FactoryGirl.create(:user, options.except(:role))
end

# -------------------------------------------------------------------------------------

def add_user_role(role)
  @user.user_roles << [FactoryGirl.build(:user_role, user: @user, role: FactoryGirl.build(:role, name: role))]
end

# -------------------------------------------------------------------------------------

def create_user_logged(options)
  create_default_user(options)
  add_user_role(options[:role]) if options[:role]
  login_as @user, :scope => :user
end

# -------------------------------------------------------------------------------------
