# encoding: utf-8
def create_default_city
  @country      = create(:country     , id: 1  , name:     'Brasil')
  @state        = create(:state       , id: 5  , uf:       'SP'     , name: 'São Paulo', country_id: @country.id)
  @city         = create(:city        , id: 538, state_id: @state.id, name: 'São Paulo')
  @neighborhood = create(:neighborhood, id: 888, city_id:  @city.id , name: 'Santana')
  @cep          = create(:cep         , id: 333, state_id: @state.id, city_id:  @city.id, neighborhood_id: @neighborhood.id)
end

def create_default_city_with_propsect_regions
  create_default_city
  create(:prospect_region, state_id: @state.id, city_id: @city.id, name: 'Zona Norte')
  create(:prospect_region, state_id: @state.id, city_id: @city.id, name: 'Zona Leste')
end

def create_complete_prospect(user, number, status)
  create_default_city
  create(:prospect,
    user_id:            user,
    phone_1:            number,
    status:             status,
    scheduled_at:       Time.now,
    reserved_at:        Time.now,
    visit_at:           Time.now,
    prospect_region_id: 1,
    proprietor:         'Aleixo',
    name:               'Editora Zeus',
    zipcode:            '02035020',
    street_name:        'Rua Doutor Zuquim',
    number:             '576',
    neighborhood:       'Santana',
    state_id:           @state.id,
    city_id:            @city.id
  )
end
