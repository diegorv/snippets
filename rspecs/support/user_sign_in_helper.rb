module SignInHelper
  def sign_in(user)
    visit root_path

    fill_in 'Email'   , with: user.email
    fill_in 'Password', with: user.password

    click_button 'Sign in'
  end

  def login_user_without_form(user)
    login_as user, :scope => :user # Bypass session form
    visit root_path
  end
end

RSpec.configure do |config|
  config.include SignInHelper, type: :feature
end
